/*Load the expressjs module into our application and saved it in a variable called express*/

const express = require("express");
// Create an application with expressjs.
// This creates an application that uses express and stores it as app
// app is our server
const app = express();

const port = 4000;

// middleware
// express.json() is a method from express which allow us to handle the streaming of data and automatically parse the incoming JSON from our req body.
// app.use is used to run a method or another function for our expressjs api
app.use(express.json());

// mock data
let users = [
    {
        username: "BMadrigal",
        email: "fateReader@gmail.com",
        password: "dontTalkAboutMe"
    },
    {
        username: "Luisa",
        email: "stronggirl@gmail.com",
        password: "pressure"
    }
];

let items = [
    {
        name: "roses",
        price: 170,
        isActive: true
    },
    {
        name: "tulips",
        price: 250,
        isActive: true
    },
];

// Express has a methods to use as routes corresponding to HTTP methods.
// app.get(<endpoint>, <function for req and res>)

app.get('/', (req, res) => {

    // Once the route is accessed, we can send a response with the use of res.send()
    // res.send() actually combines writeHead() and end().
        // used to send a response to the client and ends the request
    res.send('Hello from my first expressJS API')
    //res.status(200).send('Hello from my first expressJS API')
});

/*
    Mini Activity: 5 mins

    >> Create a get route in Expressjs which will be able to send a message in the client:

        >> endpoint: /greeting
        >> message: 'Hello from Batch182-surname'

    >> Test in postman
    >> Send your response in hangouts

*/

// S O L U T I O N
app.get('/greeting', (req, res) => {
    res.send('Hello from Batch182-Garcia');
});

//retrieval of the mock database
app.get('/users', (req, res) => {

    //res.send already stringifies for you
    res.send(users);

    // res.json(users);
});

// POST Method
app.post('/users', (req, res) => {

    console.log(req.body); // result: {}

    // simulate creating a new user document
    let newUser = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    }

    // push newUser into users array
    users.push(newUser);
    console.log(users);

    // send the updated users array in the client
    res.send(users);
});

// DELETE Method
app.delete('/users', (req, res) => {

    users.pop();
    console.log(users);

    // send the updated users array
    res.send(users);
});

// PUT Method
// update user's password
// :index - wildcard
//url: localhost:4000/users/0
app.put('/users/:index', (req, res) => {

    // empty object ({}) before putting values in the request body
    console.log(req.body)

    // an object that contains the value of URL params (referring to the wildcard in the endpoint)
    console.log(req.params)

    // parseInt the value of the number coming from req.params
    // ['0'] turns into [0]
    let index = parseInt(req.params.index);

    // users[0].password
        // pertains to the first object which BMadrigal
        // the updated password will be coming from the request body
    users[index].password = req.body.password;

    // send the updated user
    res.send(users[index]);
});
/*
    Mini-Activity: 5 mins

    >> endpoint: users/update/:index
    >> method: PUT

        >> Update a user's username
        >> Specify the user using the index in the params
        >> Put the updated username in request body
        >> Send the updated user as a response
        >> Test in Postman 
        >> Send your screenshots in Hangouts

*/
// S O L U T I O N

app.put('/users/update/:index', (req, res) => {

    let index = parseInt(req.params.index);

    users[index].username = req.body.username;

    res.send(users[index]); 
});




// RETRIEVAL OF SINGLE USER
app.get('/users/getSingleUser/:index', (req, res) => {
    console.log(req.params); //result : {index: '1'}

    // let req.params = [index: '1']
    // parseInt('2')
    let index = parseInt(req.params.index)
    console.log(index); //result : 1

    res.send(users[index]);
    console.log(users[index]);
})


// A C T I V I T Y
app.get('/items', (req, res) => {
res.send(items);
});


app.post('/items', (req, res) => {

    console.log(req.body); // result: {}

    let newItems = {
        name: req.body.name,
        price: req.body.email,
        isActive: req.body.isActive
    }



    items.push(newItems);
    console.log(items);

    res.send(items);
});

app.put('/items/:index', (req, res) => {

    let index = parseInt(req.params.index);

    items[index].price = req.body.price;

    res.send(items[index]); 
});

app.listen(port, () => console.log(`Server is running at port ${port}`))